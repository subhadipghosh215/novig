import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: any[] = [];

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get(environment.baseUrl + 'admin/getUsers', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    }).toPromise()
      .then((res: any) => {
        this.users = res.data;
      })
      .catch(console.error);
  }

}
