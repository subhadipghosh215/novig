import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private router:Router) {
    if(!localStorage.getItem('accessToken')){
      router.navigate(['/auth/login'])
        .then()
        .catch()
    }
  }

  ngOnInit(): void {
  }

  logout(){
    localStorage.clear();
    location.reload()
  }


}
