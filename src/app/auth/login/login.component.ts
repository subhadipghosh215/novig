import {Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public email = '';
  public password = '';

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  login(): void {
    if (!this.email || !this.password) {
      return alert('email and password are required');
    }
    this.http.post(environment.baseUrl + 'auth/login', {
      email: this.email,
      password: this.password
    }).toPromise()
      .then((response: any) => {
        localStorage.setItem('accessToken', response.data.token);
        location.reload();
      })
      .catch(error => {
        alert(error.error.message);
      });
  }
}
